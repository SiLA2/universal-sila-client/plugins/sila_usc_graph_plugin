package universal.client.graph.plugin;

import com.silastandard.universalclient.plugin.PluginService;
import com.silastandard.universalclient.plugin.annotations.UscPlugin;
import com.silastandard.universalclient.plugin.events.lifecycle.PluginCloseEvent;
import com.silastandard.universalclient.plugin.events.lifecycle.PluginLoadEvent;
import com.silastandard.universalclient.plugin.events.lifecycle.PluginStartEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.ComponentScan;


@Slf4j
@ComponentScan
@UscPlugin(name="Graph Plugin", version="0.8.0")
public class GraphPlugin implements PluginService {

    @Override
    public void onLoad(PluginLoadEvent event) {
        log.info("Loading graph plugin...");
    }

    @Override
    public void onStart(PluginStartEvent event) {
        log.info("Graph plugin started!");
    }

    @Override
    public void onStop(PluginCloseEvent event) {
        log.info("Graph plugin stopped.");
    }
}
