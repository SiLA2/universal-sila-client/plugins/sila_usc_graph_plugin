# sila_usc_graph_plugin

Simple plugin that display a graph of the last 10 Intermediate Responses or Observable Property Values with a Basic or Integer type

![Graph](assets/graph.gif)
