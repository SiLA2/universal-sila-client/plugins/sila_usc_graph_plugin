const webpack = require("webpack");
const WebpackAssetsManifest = require("webpack-assets-manifest");
const { BundleAnalyzerPlugin } = require("webpack-bundle-analyzer");
const remoteComponentConfig = require("./remote-component.config").resolve;

const externals = Object.keys(remoteComponentConfig).reduce(
    (obj, key) => ({
      ...obj, [key]: key
    }), {}
);

module.exports = {
  plugins: [
    new webpack.EnvironmentPlugin({
      "process.env.NODE_ENV": process.env.NODE_ENV
    }),
    new BundleAnalyzerPlugin({
      analyzerMode: "static",
      openAnalyzer: false,
      reportFilename: "webpack-bundle-analyzer-report.html"
    }),
    new WebpackAssetsManifest()
  ],
  entry: "./src/index.tsx",
  output: {
    filename: "./main.js",
    libraryTarget: "commonjs"
  },
  // Enable sourcemaps for debugging webpack's output.
  devtool: "source-map",
  optimization: {
    moduleIds: 'named',
    //minimize: true,
    //nodeEnv: 'production',
    //runtimeChunk: {
    //  name: "single",
    //},
  },
  externals: {
    ...externals,
    "remote-component.config.js": "remote-component.config.js"
  },
  resolve: {
    extensions: ["", ".webpack.js", ".web.js", ".ts", ".tsx", ".js", ".jsx", ".json", ".wasm"],
    fallback: {
      "http": false,
      "https": false,
      "os": false
    },
  },
  module: {
    rules: [
      {
        test: /\.proto/,
        type: 'asset/source', // https://webpack.js.org/guides/asset-modules/#general-asset-type
      },
      { test: /\.tsx?$/, loader: "ts-loader" },
      { test: /\.js$/, loader: "source-map-loader" },
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader"
        }
      }
    ]
  }
};
