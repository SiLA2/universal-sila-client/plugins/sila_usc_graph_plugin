import React, { useEffect, useRef } from 'react';
import Chart from 'chart.js/auto';

type Props = {
    data: string[]; // an array of integers represented as a string
};

const LineGraph: React.FC<Props> = ({ data }) => {
    const chartRef = useRef<HTMLCanvasElement>(null);

    useEffect(() => {
        if (chartRef.current) {
            const chart = new Chart(chartRef.current, {
                type: 'line',
                options: {
                    animation: {
                        duration: 0
                    }
                },
                data: {
                    labels: data.map((value, index) => `#${index + 1}`), // use index as labels
                    datasets: [
                        {
                            label: 'Values',
                            data: data.map((value) => parseInt(value)), // parse strings to integers
                            borderColor: 'rgb(75, 192, 192)',
                            tension: 0.1,
                        },
                    ],
                },
            });

            return () => {
                chart.destroy(); // clean up Chart.js instance
            };
        }
    }, [data]);

    return (
        <div>
            <canvas ref={chartRef}></canvas>
        </div>
    );
};

export default LineGraph;