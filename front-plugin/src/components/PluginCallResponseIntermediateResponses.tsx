import React from "react";
import {
    PluginCallResponseIntermediateResponsesProps,
} from "@sila2/universal-client-front/plugin/PluginImplementation";
import LineGraph from "./LineGraph";
import {findDataTypeFromIdentifier} from "@sila2/universal-client-front/components/server/call/dataTypeForm/dataType";
import {useSelector} from "react-redux";
import {getAppSettingsSelector} from "@sila2/universal-client-front/store/front/app/setting/selector";

export const PluginCallResponseIntermediateResponses = (props: PluginCallResponseIntermediateResponsesProps): JSX.Element => {
    const { darkMode } = useSelector(getAppSettingsSelector);

    const firstNumericValue = getFirstNumericValue(props);
    if (!firstNumericValue) {
        return <></>;
    }
    const values = props.response.intermediateResponse.slice(-10).map(r => r[firstNumericValue.relativeIdentifier]?.value);
    if (!values.length) {
        return <></>;
    }
    return (
        <div style={{backgroundColor: darkMode ? "#222224" : "#eeeeee"}}>
            <LineGraph data={values}></LineGraph>
        </div>
    );
}

function getFirstNumericValue(props: PluginCallResponseIntermediateResponsesProps) {
    return props.command.intermediateResponses.find((c: any) => {
        if (!c?.dataType) {
            return false;
        }
        const type = (c.dataType.dataTypeIdentifier) ? (
            findDataTypeFromIdentifier(props.feature.dataTypeDefinitions, c.dataType.dataTypeIdentifier)
        ) : (c.dataType);
        const unwrapType = (c.dataType.constrained) ? type.constrained.dataType : type;
        return unwrapType?.basic === 'INTEGER' || unwrapType?.basic === 'REAL';
    })
}