import React from "react";
import {
    PluginCallResponseObservablePropertyValuesProps,
} from "@sila2/universal-client-front/plugin/PluginImplementation";
import LineGraph from "./LineGraph";
import {
    findDataTypeFromIdentifier
} from "@sila2/universal-client-front/components/server/call/dataTypeForm/dataType";
import {useSelector} from "react-redux";
import {getAppSettingsSelector} from "@sila2/universal-client-front/store/front/app/setting/selector";
export const PluginCallResponseObservablePropertyValues = (props: PluginCallResponseObservablePropertyValuesProps): JSX.Element => {
    const { darkMode } = useSelector(getAppSettingsSelector);

    if (!isNumericValue(props)) {
        return <></>;
    }
    const values = props.response.intermediateResponse.slice(-10).map(r => r[props.property.relativeIdentifier]?.value);
    if (!values.length) {
        return <></>;
    }
    return (
        <div style={{backgroundColor: darkMode ? "#222224" : "#eeeeee"}}>
            <LineGraph data={values}></LineGraph>
        </div>
    );
}

function isNumericValue(props: PluginCallResponseObservablePropertyValuesProps) {
    if (!props?.property?.response?.dataType) {
        return false;
    }
    const type = (props.property.response.dataType.dataTypeIdentifier) ? (
        findDataTypeFromIdentifier(props.feature.dataTypeDefinitions, props.property.response.dataType.dataTypeIdentifier)
    ) : (props.property.response.dataType);
    const unwrapType = (type.constrained) ? type.constrained.dataType : type;
    return unwrapType?.basic === 'INTEGER' || unwrapType?.basic === 'REAL';
}