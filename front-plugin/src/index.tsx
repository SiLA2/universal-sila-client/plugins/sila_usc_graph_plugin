/**
 * Entrypoint of the Remote Component.
 */
import PluginImplementation from "@sila2/universal-client-front/plugin/PluginImplementation";
import {PluginCallResponseObservablePropertyValues} from "./components/PluginCallResponseObservablePropertyValues";
import {PluginCallResponseIntermediateResponses} from "./components/PluginCallResponseIntermediateResponses";

const Index: PluginImplementation = {
    PluginCallResponseObservablePropertyValues,
    PluginCallResponseIntermediateResponses
}

export { Index };
