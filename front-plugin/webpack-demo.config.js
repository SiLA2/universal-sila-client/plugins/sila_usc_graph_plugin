const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");
const webpack = require("webpack");

module.exports = {
    plugins: [
        new webpack.EnvironmentPlugin({
            "process.env.NODE_ENV": process.env.NODE_ENV
        }),
        new HtmlWebpackPlugin({
            template: "./src/index.html"
        })
    ],
    entry: {
        demo: "./src/webpack-dev-server.js"
    },
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.proto/,
                type: 'asset/source', // https://webpack.js.org/guides/asset-modules/#general-asset-type
            }
        ]
    },
    resolve: {
        extensions: ["", ".webpack.js", ".web.js", ".ts", ".tsx", ".js", ".jsx", ".json", ".wasm"],
        alias: {
            "remote-component.config.js": path.resolve("./remote-component.config.js"),
        },
        fallback: {
            "http": false,
            "https": false,
            "os": false
        },
    }
};
