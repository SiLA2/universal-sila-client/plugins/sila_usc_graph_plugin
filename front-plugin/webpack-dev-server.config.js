const webpack = require("webpack");
const path = require("path");
const WebpackAssetsManifest = require("webpack-assets-manifest");
const { BundleAnalyzerPlugin } = require("webpack-bundle-analyzer");
const remoteComponentConfig = require("./remote-component.config").resolve;
const config = require("./webpack.config");

const externals = Object.keys(remoteComponentConfig).reduce(
    (obj, key) => ({
      ...obj, [key]: key
    }), {}
);

module.exports = {
  devtool: 'source-map',
  plugins: [
    new webpack.EnvironmentPlugin({
      "process.env.NODE_ENV": process.env.NODE_ENV
    }),
    new BundleAnalyzerPlugin({
      analyzerMode: "static",
      openAnalyzer: false,
      reportFilename: "webpack-bundle-analyzer-report.html"
    }),
    new WebpackAssetsManifest(),
    new webpack.HotModuleReplacementPlugin()
  ],
  entry: {
    main: "./src/index.tsx"
  },
  output: {
    libraryTarget: "commonjs"
  },
  externals: {
    ...externals,
    "remote-component.config.js": "remote-component.config.js"
  },
  module: config[0].module,
  devServer: {
    hot: true,
    static: __dirname,
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
      "Access-Control-Allow-Headers":
        "X-Requested-With, content-type, Authorization"
    }
  },
  resolve: {
    extensions: ["", ".webpack.js", ".web.js", ".ts", ".tsx", ".js", ".jsx", ".json", ".wasm"],
    alias: {
      "remote-component.config.js": path.resolve("./remote-component.config.js")
    },
    fallback: {
      "http": false,
      "https": false,
      "os": false
    },
  }
};
