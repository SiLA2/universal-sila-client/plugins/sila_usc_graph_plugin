// Bundled dependencies 
//  byte-base64
//  pako
//  react-qr-code
//  react-qr-scanner
//  react-qrcode-logo
//  chart.js

// Common dependencies 
module.exports = {
  resolve: {
    "@mui/icons-material": require("@mui/icons-material"),
    "@mui/lab": require("@mui/lab"),
    "@mui/material": require("@mui/material"),
    "nanoevents": require("nanoevents"),
    "react": require("react"),
    "react-redux": require("react-redux"),
    "redux": require("redux"),
    "redux-logger": require("redux-logger"),
    "redux-saga": require("redux-saga"),
    "reselect": require("reselect")
  }
};
