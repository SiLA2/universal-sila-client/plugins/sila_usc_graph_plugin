const webpackMainConfig = require("./webpack-main.config");
//const webpackDemoConfig = require("./webpack-demo.config");

module.exports = [
    webpackMainConfig,
    //webpackDemoConfig uncomment to build demo with all dependency bundled, even peer dependencies
];
